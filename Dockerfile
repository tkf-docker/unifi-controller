FROM teknofile/base-ubuntu-s6:20.04

ARG TARGETPLATFORM
ARG BUILDPLATFORM
ARG UNIFI_VERSION

LABEL maintainer="teknofile <teknofile@teknofile.org>"

ARG UNIFI_BRANCH="stable"
ARG DEBIAN_FRONTEND="noninteractive"

RUN \
  apt update && apt upgrade -y && \
  apt install -y \
    binutils \
    jsvc \
    libcap2 \
    logrotate \
    wget \
    ca-certificates \
    apt-transport-https \
    curl \
    gnupg \
    openjdk-11-jre-headless \
    mongodb-server && \
    echo "**** install unifi ****" && \
    if [ -z ${UNIFI_VERSION+x} ]; then \
      UNIFI_VERSION=$(curl -sX GET http://dl-origin.ubnt.com/unifi/debian/dists/${UNIFI_BRANCH}/ubiquiti/binary-amd64/Packages \
      |grep -A 7 -m 1 'Package: unifi' \
      | awk -F ': ' '/Version/{print $2;exit}' \
      | awk -F '-' '{print $1}'); \
    fi && \
    mkdir -p /app && \
    echo "**** unifi version is: ${UNIFI_VERSION} ****" && \
    curl -o \
    /app/unifi.deb -L \
      "https://dl.ui.com/unifi/${UNIFI_VERSION}/unifi_sysvinit_all.deb" && \
    echo "**** cleanup ****" && \
    apt-get clean && \
    rm -rf \
      /tmp/* \
      /var/lib/apt/lists/* \
      /var/tmp/*

COPY root/ /

# Volumes and Ports
WORKDIR /usr/lib/unifi
VOLUME /config
EXPOSE 8080 8443 8843 8880
