pipeline {
  agent any;

  environment {
    scannerHome = tool 'TkfSonarqube'

    UPSTREAM_NAME = 'docker-unifi'
    CONTAINER_NAME = 'unifi-controller'
    
    UNIFI_BRANCH="stable"
    LOCAL_DOCKER_REGISTRY = 'artifactory.cosprings.teknofile.net/tkf-docker'
  }

  stages {
    stage('Setup Environment') {
      steps {
        script {
          env.EXIT_STATUS = ''
          env.CURR_DATE = sh(
            script: '''date '+%Y-%m-%d_%H:%M:%S%:z' ''',
            returnStdout: true).trim()
          env.GITHASH_SHORT = sh(
            script: '''git log -1 --format=%h''',
            returnStdout: true).trim()
          env.GITHASH_LONG = sh(
            script: '''git log -1 --format=%H''',
            returnStdout: true).trim()
          env.COMMIT = sh(
            script: '''git rev-parse HEAD''',
            returnStdout: true).trim()
          env.GIT_TAG_NAME = sh(
            script: '''git describe --tags ${commit}''',
            returnStdout: true).trim()
          currentBuild.displayName = "${GIT_TAG_NAME}"
        }
      }
    }
    stage('SonarQube analysis') {
      steps {
        withSonarQubeEnv('TkfSonarqube') {
          sh "${scannerHome}/bin/sonar-scanner"
        }
      }
    }
    stage('Determine latest UNIFI version') {
      steps {
        script{
          env.UNIFI_VERSION = sh(
            script: '''curl -sX GET http://dl-origin.ubnt.com/unifi/debian/dists/stable/ubiquiti/binary-amd64/Packages  | grep "Version: " | awk {' print $2 '} | awk -F"-" {'print $1'}''',
            returnStdout: true).trim()
          sh "echo ${UNIFI_VERSION}"
        }
      }
    }
    stage('Buildx') {
      agent {
        label 'x86_64'
      }
      steps {
        echo "Running on node: ${NODE_NAME}"

        withDockerRegistry(credentialsId: 'teknofile-dockerhub', url: "https://index.docker.io/v1/") {
          sh '''
            docker buildx build \
            --no-cache --pull \
            --platform linux/amd64 \
            --build-arg UNIFI_VERSION=${UNIFI_VERSION} \
            -t teknofile/${CONTAINER_NAME} \
            -t teknofile/${CONTAINER_NAME}:latest \
            -t teknofile/${CONTAINER_NAME}:${GITHASH_SHORT} \
            -t teknofile/${CONTAINER_NAME}:unifi-${UNIFI_VERSION} \
            . \
            --push
          '''
        }
        withDockerRegistry(url: 'https://artifactory.cosprings.teknofile.net/tkf-docker', credentialsId: 'tkf-jenkins-artifactory') {
          sh '''
            docker pull teknofile/${CONTAINER_NAME}:unifi-${UNIFI_VERSION}
            docker tag teknofile/${CONTAINER_NAME}:unifi-${UNIFI_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}:unifi-${UNIFI_VERSION}
            docker tag teknofile/${CONTAINER_NAME}:unifi-${UNIFI_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}:latest
            docker tag teknofile/${CONTAINER_NAME}:unifi-${UNIFI_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}
            docker push ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME} -a
          '''
        }
      }
    }
    stage('Update Helm Chart') {
      steps {
        script {
          sh '''
            # Update the Chart Version with the tag from git
            sed -i 's/version:.*/version: '"$GIT_TAG_NAME"'/g' helm/Chart.yaml

            # Update Chart App Version with the version of Unifi
            sed -i 's/appVersion:.*/appVersion: '"${UNIFI_VERSION}"'/g' helm/Chart.yaml

            helm lint helm

            helm package helm
            helm push-artifactory *.tgz tkf-charts
          '''
        }
      }
    }
  }
  post {
    always {
      echo 'Cleaning up.'
      deleteDir() /* clean up our workspace */
    }
  }
}
